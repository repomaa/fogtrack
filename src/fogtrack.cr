require "./fogtrack/api"
require "./fogtrack/working_on_api"

module Fogtrack
  private def self.api
    @@api ||= API.new
  end

  private def self.working_on_api
    @@working_on_api ||= WorkingOnAPI.new
  end

  def self.login(email, password)
    api.login(email, password)
  end

  def self.start_tracking(case_number)
    api.start_tracking(case_number)
  end

  def self.start_tracking(kase : Case)
    start_tracking(kase.case_number)
  end

  def self.stop_tracking
    api.stop_tracking
  end

  def self.resume_tracking
    working_on = working_on_api.working_on
    return if working_on.active?
    start_tracking(working_on.recent.first)
  end

  def self.intervals(from = nil, to = nil, limit = Int32::MAX, unique = false)
    result = api.list_intervals(from, to).intervals.each.reject(&.deleted?).first(limit)

    if unique
      result = result.group_by(&.case_number).map do |_, intervals|
        intervals.max_by(&.id)
      end
    end

    result.to_a.sort_by!(&.id).reverse_each
  end

  def self.recent
    working_on_api.working_on.all
  end

  def self.current
    working_on_api.working_on.active
  end

  def self.tracking?
    working_on_api.working_on.active?
  end
end
