require "../authorized_request"

module Fogtrack::APIRequests
  class StopTracking < AuthorizedRequest
    def initialize
      super("stopWork")
    end

    def parse_response(body)
      APIResponse(ResponseData).from_json(body)
    end

    # Empty data
    class ResponseData
      include JSON::Serializable
    end
  end
end
