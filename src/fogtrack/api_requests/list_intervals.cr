require "../authorized_request"
require "../time_interval"

module Fogtrack::APIRequests
  class ListIntervals < AuthorizedRequest
    @[JSON::Field(key: "dtStart")]
    @from : Time?
    @[JSON::Field(key: "dtEnd")]
    @to : Time?

    def initialize(@from = nil, @to = nil)
      super("listIntervals")
    end

    def parse_response(body)
      APIResponse(ResponseData).from_json(body)
    end

    class ResponseData
      include JSON::Serializable

      getter! intervals : Array(TimeInterval)?
    end
  end
end
