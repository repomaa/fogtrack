module Fogtrack
  class RequestError < Exception
    def initialize(code : Int32, errors : Array(APIError))
      super(build_message(code, errors))
    end

    def build_message(code, errors)
      String.build do |builder|
        builder.puts("[#{code}] #{HTTP::Status.new(code).description}")
        errors.each do |error|
          builder.print "  #{error.code} - #{error.message}"
          error.detail.try { |detail| builder.print " - #{detail}" }
          builder.puts
        end
      end
    end
  end
end
