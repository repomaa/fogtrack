require "rofi"
require "csv"
require "option_parser"
require "../fogtrack"
require "./time_interval"

module Fogtrack
  module CLI
    GENERAL_PARSER = OptionParser.new do |p|
      p.banner = <<-BANNER
        Usage: #{PROGRAM_NAME} command [options]

        Commands:
          login      Outputs a token you can use with fogtrack
          start      Starts tracking on a case
          stop       Stops tracking
          resume     Resumes tracking on case last tracked on
          ls/list    Lists tracked intervals
          recent     List cases recently tracked on
          current    Show case currently tracked on (if any)
          dialog     Display a rofi dialog to choose from cases to track on
          rofi-mode  Use with `rofi -modi 'fogtrack:#{PROGRAM_NAME} rofi-mode' -show rofi-mode`

        For more info about an individual command, run
        #{PROGRAM_NAME} <command> --help

        Environment Variables:

        FOGTRACK_HOST   A URL to the host of your fogbugz instance (e.g. https://myfogbugz.com)
        FOGTRACK_TOKEN  An API token you have either generated in the UI or gotten with the login command"

        BANNER

      p.on("-h", "--help", "Display this message") do
        puts p
        exit
      end
    end

    def self.sanitize_format(format)
      format.gsub(/(?<!\\)((?:\\{2})*)\\n/, "\\1\n").gsub("\\\\n", "\\n")
    end

    def self.start_tracking
      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} start <case number>"

        p.on("-h", "--help", "Display this message") do
          puts p
          exit
        end
      end

      parser.parse!
      case_number = ARGV[1]?.try(&.to_i)
      abort("Error: Missing case number\n#{parser}") if case_number.nil?
      abort("Error: invalid option(s)\n#{parser}") if ARGV.size > 2
      Fogtrack.start_tracking(case_number)
    end

    def self.stop_tracking
      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} stop"

        p.on("-h", "--help", "Display this message") do
          puts p
          exit
        end
      end

      parser.parse!
      abort("Error: invalid option(s)\n#{parser}") if ARGV.size > 1
      Fogtrack.stop_tracking
    end

    def self.resume_tracking
      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} resume"

        p.on("-h", "--help", "Display this message") do
          puts p
          exit
        end
      end

      parser.parse!
      abort("Error: invalid option(s)\n#{parser}") if ARGV.size > 1
      Fogtrack.resume_tracking
    end

    def self.login
      # Set to empty value to allow config parsing
      ENV["FOGTRACK_TOKEN"] = ""

      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} login"

        p.on("-h", "--help", "Display this message") do
          puts p
          exit
        end
      end

      parser.parse!
      abort("Error: invalid option(s)\n#{parser}") if ARGV.size > 1

      print "Email: "

      until email = gets(chomp: true)
        puts "Cannot be left blank"
        print "Email: "
      end

      print "Password (won't be echoed): "

      until password = STDIN.noecho { gets(chomp: true) }
        puts "Cannot be left blank"
        print "Password (won't be echoed): "
      end

      puts

      token = Fogtrack.login(email, password).token
      puts "Success! Add this to your environment:"
      puts "FOGTRACK_TOKEN=#{token}"
    end

    def self.intervals
      from = nil
      to = nil
      format = TimeInterval.format_string
      time_format = TimeInterval.time_format
      limit = Int32::MAX
      unique = false
      sum = false

      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} ls"

        p.on("-f DATE", "--from=DATE", "Filter result to display only intervals after (ISO 8601 formatted) DATE") do |date|

          from = Time.parse_local(date, "%F")
        end

        p.on("-t DATE", "--to=DATE", "Filter result to display only intervals until (ISO 8601 formatted) DATE") do |date|
          to = Time.parse_local(date, "%F")
        end

        p.on("-l COUNT", "--limit=COUNT", "Limit results to COUNT items (default: unlimited)") do |number|
          limit = number.to_i
        end

        p.on("-o FORMAT", "--output=FORMAT", "Display the results in the given FORMAT (default: #{format.inspect})") do |string|
          format = string
        end

        p.on("-u", "--unique", "Only show one item (the most recent) per case") do
          unique = true
        end

        p.on("--time-format=FORMAT", "Output times in FORMAT (default: #{time_format})") do |string|
          time_format = string
        end

        p.on("-s", "--sum", "Sum and print out the total duration of the intervals") do
          sum = true
        end

        p.on("-h", "--help", "Display this message") do
          puts p
          exit
        end

        {% begin %}
        p.separator(
          "Possible format variables:\n#{TimeInterval.attributes.map { |attribute| "  %{#{attribute}}" }.join('\n') }"
        )
        {% end %}
      end

      parser.parse!
      abort("Error: invalid option(s)\n#{parser}") if ARGV.size > 1

      TimeInterval.format_string = sanitize_format(format)
      TimeInterval.time_format = time_format

      total_duration = Time::Span.zero

      Fogtrack.intervals(from, to, limit, unique).first(limit).each do |interval|
        print interval
        total_duration += interval.duration if sum
      end

      return unless sum
      pretty_duration = "%d:%02d:%02d" % [
        total_duration.days * 24 + total_duration.hours,
        total_duration.minutes,
        total_duration.seconds
      ]
      puts "\nTotal duration: #{pretty_duration}"
    end

    def self.recent
      format = Case.format_string

      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} recent"

        p.on("-o FORMAT", "--output=FORMAT", "Display the results in the given FORMAT (default: #{format.inspect})") do |string|
          format = string
        end

        {% begin %}
        p.separator(
          "Possible format variables:\n#{Case.attributes.map { |attribute| "  %{#{attribute}}\n" }}"
        )
        {% end %}

        p.on("-h", "--help", "Display this message") do
          puts p
          exit
        end
      end

      parser.parse!
      abort("Error: invalid option(s)\n#{parser}") if ARGV.size > 1

      Case.format_string = sanitize_format(format)

      Fogtrack.recent.each do |current_case|
        print current_case
      end
    end

    def self.current
      format = "%{case_number} %{title}\\n"

      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} current"

        p.on("-o FORMAT", "--output=FORMAT", "Display the results in the given FORMAT (default: #{format})") do |string|
          format = string
        end

        {% begin %}
        p.separator(
          "Possible format variables:\n#{Case.attributes.map { |attribute| "  %{#{attribute}}\n" }}"
        )
        {% end %}

        p.on("-h", "--help", "Display this message") do
          puts p
          exit
        end
      end

      parser.parse!
      abort("Error: invalid option(s)\n#{parser}") if ARGV.size > 1
      current_case = Fogtrack.current

      if current_case.nil?
        STDERR.puts "Not tracking"
        return
      end

      Case.format_string = sanitize_format(format)
      print current_case
    end

    private def self.rofi_data(from, limit)
      current = Channel(Case?).new(1)
      spawn { current.send(Fogtrack.current) }

      cases = Fogtrack.intervals(from, nil, limit, true).to_a.map(&.to_case)
      Case.format_string = Case.format_string.chomp

      if current_case = current.receive
        urgent_rows = cases.index(current_case).try { |index| [index] }
      end

      choices = cases + ["## Stop tracking ##"]
      active_rows = [choices.size - 1]

      { choices: choices, urgent_rows: urgent_rows, active_rows: active_rows }
    end

    private def self.handle_rofi_choice(choice)
      case choice
      when nil then abort("Cancelled dialog")
      when "## Stop tracking ##" then Fogtrack.stop_tracking
      when /^\d+/ then Fogtrack.start_tracking(choice[/^\d+/].to_i)
      else abort(%|Invalid selection "#{choice}"|)
      end
    end

    def self.dialog
      from = nil
      limit = Int32::MAX

      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} dialog"

        p.on("-f DATE", "--from=DATE", "Filter result to display only intervals after (ISO 8601 formatted) DATE") do |date|

          from = Time.parse_local(date, "%F")
        end

        p.on("-l COUNT", "--limit=COUNT", "Limit results to COUNT items (default: unlimited)") do |number|
          limit = number.to_i
        end
      end

      parser.parse!
      abort("Error: invalid option(s)\n#{parser}") if ARGV.size > 1

      rofi_data = rofi_data(from, limit)

      dialog = Rofi::Dialog.new(
        rofi_data[:choices],
        prompt: "Switch to",
        case_insensitive: true,
        urgent_rows: rofi_data[:urgent_rows],
        active_rows: rofi_data[:active_rows],
        no_custom: true
      )

      result = dialog.show
      abort("Cancelled dialog") if result.nil?

      choice = result.selected_entry
      choice.is_a?(Case) ? Fogtrack.start_tracking(choice) : Fogtrack.stop_tracking
    end

    def self.rofi_mode
      from = nil
      limit = Int32::MAX

      parser = OptionParser.new do |p|
        p.banner = "Usage: #{PROGRAM_NAME} ls"

        p.on("-f DATE", "--from=DATE", "Filter result to display only intervals after (ISO 8601 formatted) DATE") do |date|

          from = Time.parse_local(date, "%F")
        end

        p.on("-l COUNT", "--limit=COUNT", "Limit results to COUNT items (default: unlimited)") do |number|
          limit = number.to_i
        end
      end

      parser.parse!
      if ARGV.size < 2
        rofi_data = rofi_data(from, limit)
        print "\x00prompt\x1fSwitch to\n"
        rofi_data[:urgent_rows].try do |urgent_rows|
          if urgent_rows.any?
            print "\0urgent\x1f#{urgent_rows.join(",")}\n"
          end
        end

        if rofi_data[:active_rows].any?
          print "\0active\x1f#{rofi_data[:active_rows].join(",")}\n"
        end

        rofi_data[:choices].each do |choice|
          puts choice
        end
      else
        handle_rofi_choice(ARGV[1])
      end
    end

    def self.run
      command = ARGV.first?
      case command
      when "login" then login
      when "start" then start_tracking
      when "stop" then stop_tracking
      when "resume" then resume_tracking
      when "ls", "list" then intervals
      when "recent" then recent
      when "current" then current
      when "dialog" then dialog
      when "rofi-mode" then rofi_mode
      when nil
        GENERAL_PARSER.parse!
        abort("Error: Missing command #{command}\n#{GENERAL_PARSER}")
      else abort("Error: Unknown command #{command}\n#{GENERAL_PARSER}")
      end
    rescue error : Exception
      {% unless flag?(:release) %}
        raise error
      {% else %}
        abort("Error: #{error.message}")
      {% end %}
    end
  end
end

Fogtrack::CLI.run
