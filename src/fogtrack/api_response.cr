require "json"
require "./api_error"

module Fogtrack
  class APIResponse(T)
    include JSON::Serializable

    getter data : T
    getter errors : Array(APIError)
    @[JSON::Field(key: "errorCode")]
    getter error_code : Int32?
  end
end
