require "./request_error"
require "./config"
require "./client"
require "./api_response"
require "./case"

module Fogtrack
  class WorkingOnAPI
    BASE_PATH = "/f/api/0/workingon"

    def working_on
      query = HTTP::Params.build do |q|
        q.add("token", Fogtrack.config.token)
      end

      path = "#{BASE_PATH}?#{query}"

      {% unless flag?(:release) %}
        STDERR.puts "GET #{path}"
      {% end %}

      Fogtrack.client.get(path) do |response|
        parsed_response = APIResponse(ResponseData).from_json(response.body_io)
        error_code = parsed_response.error_code
        unless error_code.nil?
          raise RequestError.new(error_code, parsed_response.errors)
        end

        parsed_response.data
      end
    end

    class ResponseData
      include JSON::Serializable

      getter active : Case?
      getter recent : Array(Case)

      def active?
        !active.nil?
      end

      def all
        active = self.active
        return recent if active.nil?
        [active] + recent
      end
    end
  end
end
