require "json"

module Fogtrack
  class APIError
    include JSON::Serializable

    getter message : String
    getter detail : String?
    getter code : String
  end
end
