require "json"
require "./config"
require "./api_request"

module Fogtrack
  abstract class AuthorizedRequest < APIRequest
    @token : String

    def initialize(command)
      @token = Fogtrack.config.token
      super(command)
    end
  end
end
