require "./case"

module Fogtrack
  class TimeInterval < Case
    class_property format_string = "%{id} %{case_number} %{title}\n"
    class_property time_format = "%F %H:%M"

    @[JSON::Field(key: "ixInterval")]
    getter id : Int32
    @[JSON::Field(key: "dtStart")]
    getter from : Time
    @[JSON::Field(key: "dtEnd", converter: Fogtrack::TimeInterval::TimeOrNilConverter)]
    getter to : Time?
    @[JSON::Field(key: "fDeleted")]
    getter? deleted : Bool

    @[JSON::Field(ignore: true)]
    property format_string = "%{id} %{case_number} %{title}\n"

    def initialize(@id, case_number, @from, @to, @deleted, title)
      super(case_number, title)
    end

    def to_case
      Case.new(case_number, title)
    end

    def duration
      (to || Time.local.at_beginning_of_second) - from
    end

    def ==(other : self)
      id = other.id
    end

    def hash
      id.hash
    end

    def self.attributes
      super + %i[id from to deleted duration]
    end

    def to_named_tuple
      super().merge(
        id: id,
        from: from.to_local.to_s(self.class.time_format),
        to: to.try { |time| time.to_local.to_s(self.class.time_format) },
        deleted: @deleted,
        duration: duration
      )
    end

    module TimeOrNilConverter
      def self.from_json(parser)
        value = parser.read_string_or_null
        return nil if value.nil? || value.empty?
        Time.parse_iso8601(value)
      end
    end
  end
end
