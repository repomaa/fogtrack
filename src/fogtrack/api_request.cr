require "json"
require "./api_response"
require "./config"

module Fogtrack
  abstract class APIRequest
    include JSON::Serializable

    @[JSON::Field(key: "cmd")]
    @command : String

    def initialize(@command)
    end

    abstract def parse_response(body : IO | String) : APIResponse
  end
end
