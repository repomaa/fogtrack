require "uri"
require "env_config"

module Fogtrack
  class Config
    include EnvConfig

    getter host : String
    getter token : String

    def host
      URI.parse(@host)
    end
  end

  def self.config
    @@config ||= Config.new(ENV, prefix: "FOGTRACK")
  end
end
