require "./config"

module Fogtrack
  def self.client
    @@client ||= HTTP::Client.new(config.host)
  end
end
