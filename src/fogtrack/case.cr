require "json"

module Fogtrack
  class Case
    include JSON::Serializable

    class_property format_string = "%{case_number} %{title}\n"

    @[JSON::Field(key: "ixBug")]
    getter case_number : Int32
    @[JSON::Field(key: "sTitle")]
    getter title : String

    def initialize(@case_number, @title)
    end

    def ==(other : self)
      case_number = other.case_number
    end

    def hash
      case_number.hash
    end

    def self.attributes
      %i[case_number title]
    end

    def to_s(io)
      io.printf(self.class.format_string, to_named_tuple)
    end

    def to_named_tuple
      {
        case_number: case_number,
        title: title
      }
    end
  end
end
