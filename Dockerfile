FROM jreinert/crystal-alpine:0.29.0

RUN mkdir -p /opt/fogtrack
WORKDIR /opt/fogtrack

ADD shard.yml ./
RUN shards install --production
ADD src ./src
RUN shards build --production --no-debug --static --release fogtrack
