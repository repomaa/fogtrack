# fogtrack

A cli tool for all your fogbugz tracking needs

## Installation

### From source

1. Clone this repo
2. Cd into it
3. run `shards build --production --release`
4. copy the compiled binary from `bin/` into some directory to your `$PATH`

### From precompiled statically linked binary

1. Download the latest release artifact from (<https://gitlab.com/jreinert/fogtrack/tags>)
2. Copy the binary from the unzipped artifact to you `$PATH`

### From AUR

1. Install `fogtrack` with your favourite aur helper

## Usage

```
Usage: fogtrack command [options]

Commands:
  login      Outputs a token you can use with fogtrack
  start      Starts tracking on a case
  stop       Stops tracking
  resume     Resumes tracking on case last tracked on
  ls/list    Lists tracked intervals
  recent     List cases recently tracked on
  current    Show case currently tracked on (if any)
  dialog     Display a rofi dialog to choose from cases to track on
  rofi-mode  Use with `rofi -modi 'fogtrack:fogtrack rofi-mode' -show rofi-mode`

For more info about an individual command, run
fogtrack <command> --help

Environment Variables:

FOGTRACK_HOST   A URL to the host of your fogbugz instance (e.g. https://myfogbugz.com)
FOGTRACK_TOKEN  An API token you have either generated in the UI or gotten with the login command"

    -h, --help                       Display this message
```

## Contributing

1. Fork it (<https://gitlab.com/jreinert/fogtrack/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Merge Request

## Contributors

- [jreinert](https://gitlab.com/jreinert) Joakim Reinert - creator, maintainer

<!-- vim: spelllang=en -->
